import os.path

from aws_cdk.aws_s3_assets import Asset

from aws_cdk import (
    aws_ec2 as ec2,
    aws_iam as iam,
    aws_s3 as s3,
    core    
)

dirname = os.path.dirname(__file__)


class EC2InstanceStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # VPC
        vpc = ec2.Vpc(self, "VPC",
            nat_gateways=0,
            subnet_configuration=[ec2.SubnetConfiguration(name="public",subnet_type=ec2.SubnetType.PUBLIC)]
        )
        # Security Group
        sg = ec2.SecurityGroup(
            self,
            "instance-sg",
            vpc=vpc,
            allow_all_outbound=True,
            security_group_name="Instance SG"
        )
        sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(22)
        )
        sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(8887)
        )
        sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp_range(
                start_port=1024,
                end_port=65535
            )
        )
        # IAM Role
        role = iam.Role(
            self,
            "Role-S3-Bucket-Policy",
            assumed_by=iam.ServicePrincipal("ec2.amazonaws.com")
        )
        role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonS3FullAccess"))
        # Machine Image
        amzn_linux2 = ec2.MachineImage.latest_amazon_linux(
            generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX,
            edition=ec2.AmazonLinuxEdition.STANDARD,
            virtualization=ec2.AmazonLinuxVirt.HVM,
            storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE
        )
        # EC2 Instance
        instance = ec2.Instance(
            self,
            "API Server",
            instance_type=ec2.InstanceType("t2.micro"),
            machine_image=amzn_linux2,
            vpc=vpc,
            role=role,
            security_group=sg
        )

        bucket = s3.Bucket(
            self,
            "Bucket Policy Quiz",
            block_public_access = s3.BlockPublicAccess(
                block_public_acls=False,
                block_public_policy=False,
                ignore_public_acls=False,
                restrict_public_buckets=False
            ),
            versioned=True,
            website_index_document = "index.html"
        )


app = core.App()
EC2InstanceStack(app, "aws-jam-s3-bucket-policy")

app.synth()